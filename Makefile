ONS_PATH := ../ONS

all: main external_teacher generate_automata reachability

main: nominal_learning.cpp automata.hpp ONS_io.hpp ONS_nomset_extra.hpp ONS_quotients.hpp
	$(CXX) $(CXXFLAGS) -std=c++11 -O2 -DNDEBUG -o main nominal_learning.cpp -I. -isystem$(ONS_PATH)/include/

external_teacher: external_teacher.cpp automata.hpp examples.hpp
	$(CXX) $(CXXFLAGS) -std=c++14 -g -O2 -DDEBUG -o external_teacher external_teacher.cpp -I. -isystem$(ONS_PATH)/include/

generate_automata: generate_automata.cpp
	$(CXX) $(CXXFLAGS) -std=c++11 -g -O2 -DDEBUG -o generate_automata generate_automata.cpp -I. -isystem$(ONS_PATH)/include/

reachability: reachability.cpp
	$(CXX) $(CXXFLAGS) -std=c++14 -g -O2 -DDEBUG -o reachability reachability.cpp -I. -isystem$(ONS_PATH)/include/
