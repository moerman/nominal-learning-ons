#pragma once

#include <ostream>
#include <vector>

#include <ONS/eqimap.h>
#include <ONS/nomset.h>
#include <ONS/orbit.h>

namespace ONS {

    /* Output for nominal sets (for debugging) */
    template <typename T>
    std::ostream & operator<<(std::ostream & out, nomset<T> const & set) {
        if (set.size() == 0) {
            return out << "{}";
        }

        // This is safe. But nomset should provide const iterators at some point
        auto set2 = const_cast<nomset<T>&>(set);

        auto it = set2.begin();
        out << '{' << it->getElement();
        it++;
        while (it != set2.end()) {
            out << ", " << it->getElement();
            it++;
        }
        return out << '}';
    }

    /* Output for nominal sets (for debugging) */
    template <typename D, typename R>
    std::ostream & operator<<(std::ostream & out, eqimap<D, R> const & fun) {
        // This is safe. But eqimap should provide const iterators at some point
        auto fun2 = const_cast<eqimap<D, R>&>(fun);

        auto it = fun2.begin();
        auto e = fun2.end();
        if(it == e) return out << "{}";

        static const auto printv = [&out](std::vector<bool> const & v) {
            for(auto b : v) out << (int)b;
        };

        out << '{' << it->first.getElement() << "-"; printv(it->second.second); out << "->" << it->second.first.getElement();
        it++;
        while (it != e) {
            out << ", " << it->first.getElement() << "-"; printv(it->second.second); out << "->" << it->second.first.getElement();
            it++;
        }
        return out << '}';
    }
}
