#pragma once

#include <utility>
#include <vector>

#include <ONS/abstract.h>
#include <ONS/eqimap.h>
#include <ONS/nomset.h>
#include <ONS/orbit.h>

namespace ONS{
    template <typename T>
    nomset<T> make_set(orbit<T> const & x) {
        nomset<T> s;
        s.insert(x);
        return s;
    }

    template <typename T>
    nomset<T> make_set(T const & x) {
        nomset<T> s;
        s.insert(x);
        return s;
    }
}
