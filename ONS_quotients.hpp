#pragma once

#include <utility>
#include <vector>

#include <ONS/abstract.h>
#include <ONS/eqimap.h>
#include <ONS/nomset.h>
#include <ONS/orbit.h>

#include "ONS_nomset_extra.hpp"

namespace ONS {
    template <typename Q>
    using eq_rel = nomset<std::pair<Q, Q>>;

    template <typename Q>
    using quotient_set = nomset<std::pair<unsigned, abstract>>;
    template <typename Q>
    using quotient_map = eqimap<Q, std::pair<unsigned, abstract>>;
    template <typename Q>
    using quotient = std::pair<quotient_set<Q>, quotient_map<Q>>;

    template <typename Q>
    quotient<Q> partition_from_eqrel(eq_rel<Q> const & equiv, nomset<Q> const & elements) {
        quotient_set<Q> new_elements;
        quotient_map<Q> partition;
        unsigned curLabel = 0;

        // TODO: remove const_cast as soon as ONS has const_iterators
        auto & elements2 = const_cast<nomset<Q>&>(elements);

        for (const auto o : elements2) {
            nomset<Q> curOrbit = make_set(o);
            bool handled = false;

            for (const auto s : elements2) {
                if (!(s < o)) break;

                for (auto p : nomset_product(make_set(s), curOrbit)) {
                    if (equiv.contains(p)) {
                        const auto el = p.getElement();
                        const auto seqRef = s.getSeqFromElement(el.first);
                        const auto seqCur = o.getSeqFromElement(el.second);
                        std::vector<bool> refMask = partition.mapData[s].second;
                        std::vector<bool> curMask(o.supportSize(), false);
                        
                        size_t i_A = 0, i_B = 0;
                        while (i_A < seqRef.size() && i_B < seqCur.size()) {
                            if (seqRef[i_A] == seqCur[i_B]) {
                                if (refMask[i_A])
                                    curMask[i_B] = true;
                                i_A++;
                                i_B++;
                            } else if (seqRef[i_A] < seqCur[i_B]) {
                                i_A++;
                            } else {
                                i_B++;
                            }
                        }
                        partition.mapData[o] = {partition.mapData[s].first, curMask};
                        
                        handled = true;
                        break;
                    }
                }
                if (handled) break;
            }
            
            if (handled) continue;
            
            std::vector<bool> mask(o.supportSize(), true);
            for (const auto p : nomset_product(curOrbit, curOrbit)) {
                if (equiv.contains(p)) {
                    const auto el = p.getElement();
                    const auto seqA = o.getSeqFromElement(el.first);
                    const auto seqB = o.getSeqFromElement(el.second);
                    
                    for (size_t i = 0; i < seqA.size(); i++) {
                        if (seqA[i] != seqB[i])
                            mask[i] = false;
                    }
                }
            }
            
            unsigned tgtSupSize = 0;
            for (auto b : mask) {
                if (b) tgtSupSize++;
            }
            
            const orbit<abstract> orbitWithSupSize(tgtSupSize);
            const orbit<std::pair<unsigned, abstract>> newOrbit = orbit<std::pair<unsigned, abstract>>(std::make_pair(curLabel, orbitWithSupSize.getElement()));
            
            new_elements.insert(newOrbit);
            partition.mapData[o] = {newOrbit, mask};
            curLabel++;
        }

        return {new_elements, partition};
    }

}
