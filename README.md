Learning Nominal Automata
=========================

This is an implementation of the L-star algorithm for nominal automata.
It uses the `ONS` library of David Venhoek, which provides a very
concrete representation, useful for automata learning purposes.
The library can be found on [GitHub](https://github.com/davidv1992/ONS).

First `make` the executables. Should work with any `c++14` compiler
as long as you have the `ONS` library in the right place (see Makefile).
(The learning algorithm can be built with just c++11.)

Then use the `run.sh` script for easy testing. Note that the teacher
is external, i.e. queries are answered by a separate program. You
have to specify which language you want to learn to this separate
program. For example `run.sh DoubleWord 2` will learn the language
{ ww | length of w = 2 }. These are currently supported:

* `Fifo n`: the Fifo language (see paper)
* `DoubleWord n`: the language { ww | length of w is n }
* `DoubleWordOrd n`: the language { ww' | length of w and w' is n
  and w'i <= wi for all i }.
* `NLast n`: the language a A* a A^n.
* `NLastOrd n`: the language a A* b A^n where b <= a.
* `Interval`: the interval language (see paper)
* `Max`: the language { wa | a = max(w_1, ..., w_n) }
* `Max2`: the language { wa | a >= all elements in w } + { eps }
* `File f`: load automaton from file

The L-star implementation written in NLambda understands the same protocol.
This way, one can compare the two implementations. That nominal L-star
implementation can be found [here](https://github.com/Jaxan/nominal-lstar).

Notes:

* You can choose how counter examples are handled: adding prefixes
  or adding suffixes.
* To compare against the NLambda implementation, have a look at the `README.md`
  in the `nlambda-utils` directory.

Experiments were performed with the following setup.
```
CPU-info
Intel(R) Core(TM) i5-3470 @ 3.20GHz
8GB RAM

Software versions
Ubuntu 17.10
GCC 7.2.0
Stack lts-9.8 (includes GHC 8.0.2)
Z3 4.4.1

Package versions
nlambda commit 744b4d3
lois commit 9fe3d85
nominal-lstar commit 96ae5ba
```
