#pragma once

#include <utility>
#include <vector>

#include <ONS/eqimap.h>
#include <ONS/nomset.h>
#include <ONS/rational.h>

using namespace ONS;

/* Deterministic nominal automaton */
template<typename Q, typename A>
struct automaton {
    nomset<A> alphabet;
    nomset<Q> states;
    eqimap<std::pair<Q, A>, Q> delta;
    nomset<Q> finalStates;
    Q initialState;
    
    bool accepts(std::vector<A> const & word) const {
        Q currentState = initialState;
        for (auto const & a : word) {
            currentState = delta({currentState, a});
        }
        return finalStates.contains(currentState);
    }
};

template <typename A>
std::vector<A> concat(std::vector<A> w, std::vector<A> const & w2){
    w.insert(w.end(), w2.begin(), w2.end());
    return w;
}

template <typename A>
std::vector<A> concat(std::vector<A> w, A const & a){
    w.push_back(a);
    return w;
}

template <typename A>
std::vector<A> concat(A const & a, std::vector<A> w){
    w.insert(w.begin(), a);
    return w;
}
        
