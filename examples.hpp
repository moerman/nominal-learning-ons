#pragma once

#include <deque>
#include <fstream>
#include <map>
#include <utility>
#include <vector>

#include <ONS/abstract.h>
#include <ONS/rational.h>
#include <ONS/orbit.h>
#include <ONS/nomset.h>

#include "automata.hpp"

/* In this file we code some example regular languages. We do not represent
   them as automata, instead we simply code them imperatively. We can still
   learn the automata if we wish. For each example, we give the number of
   orbits in the nominal automata, together with the size of the support of
   each orbit. This fully describes the size of an automaton in the total
   order symmetry (for the equality symmetry we should also include the local
   symmetries).

   Most of the examples are from the Learning Nominal Automata paper. But
   some new ones have been introduce to make use of the ordering on the
   rationals. For most examples we do not need any nominal computation,
   we simply implement them in c++. But for simulating random automata, we
   use the ONS library.
*/

using namespace ONS;

template <typename T>
struct alphabet_name {
    static const std::string name;
};

/* For the moment we support two types of alphabets. */
template <>
const std::string alphabet_name<rational>::name = "ATOMS";
template <>
const std::string alphabet_name<std::pair<bool, rational>>::name = "FIFO";


/* The alphabet is {Put, Get} x Atoms. And the language describes all valid
   traces through a Fifo-queue of size n. It is a safety language.
   1 + sum_{i=0}^{n} B_i states: 0, 0, 1, 2, 3, ..., n
   where B_i are the Bell numbers, replace by ordered Bell numbers in the
   total order symmetry. */
struct fifo {
    // Put(Q) + Get(Q)
    using alphabet = std::pair<bool, rational>;

    int size;
    fifo(int size_) : size(size_) {}

    bool is_accepted(std::vector<alphabet> const & w) const {
        std::deque<rational> queue;
        
        for(auto x : w){
            // Get on an empty queue
            if(x.first && queue.empty()) return false;

            // Get with different element on front
            if(x.first && x.second != queue.front()) return false;

            // If we Put on a big queue, we crash
            if(!x.first && queue.size() >= size) return false;

            if(x.first){
                queue.pop_front();
            } else {
                queue.push_back(x.second);
            }
        }
        return true;
    }
};

/* The language { ww | w is of length n }. Very easy language, but a DFA
   accepting iit is quite large. It's actually better as a nominal DFA than
   as a ordinary DFA on a big alphabet.
   1 + B_n + 2 * sum_{i=0}^{n-1} B_i states: 0, 0, 1, 2, 3, .., n, .., 2, 1, 0
   where B_i is the i-th Bell number, replace by ordered Bell numbers in the
   total order symmetry. */
struct double_word {
    using alphabet = rational;

    int size;
    bool use_ord;

    double_word(int size_, bool use_ord_)
    : size(size_)
    , use_ord(use_ord_)
    {}

    bool is_accepted(std::vector<alphabet> const & w) const {
        if(w.empty()) return size == 0;

        std::deque<rational> first_half;
        bool first_phase = true;
        
        for(auto x : w){
            if(first_phase) {
                first_half.push_back(x);
                if(first_half.size() == size)
                    first_phase = false;
            } else {
                // Second half is too big
                if(first_half.empty()) return false;

                // Check if second half is equal (or lesser than) first half
                if(use_ord){
                    if(x > first_half.front()) return false;
                } else {
                    if(x != first_half.front()) return false;
                }

                first_half.pop_front();
            }
        }
        return first_half.empty();
    }
};

/* The language { aw | the n-last symbol in w is a }. This is a typical example
   of a language accepted by a small NFA, but big DFA.
   2^{n+1} + 1 states: 0, 1, 1, ..., 1 */
struct n_last {
    using alphabet = rational;

    int size;
    bool use_ord;

    n_last(int size_, bool use_ord_)
    : size(size_)
    , use_ord(use_ord_)
    {}

    bool is_accepted(std::vector<alphabet> const & w) const {
        // we need a distinguished element and some symbol, so size >= 2
        if(w.size() < 2) return false;

        // there is no n-last symbol (first symbol does not count)
        if(w.size() <= size + 1) return false;

        if(use_ord)
            return w[w.size() - size - 1] <= w.front();
        else
            return w[w.size() - size - 1] == w.front();
    }
};

/* The "interval" language: all prefixes of the set
   { w | [w_{i}, w_{i+1}] strictly contains [w_{i+2}, w_{i+3}] for all odd i}
   That is, words consists of intervals, and are accepted if the next interval
   is strictly smaller than the previous.
   5 states: 0, 0, 1, 2, 2 */
struct interval_lang {
    using alphabet = rational;

    bool is_accepted(std::vector<alphabet> const & w) const {
        enum {
            init,
            first,
            interval,
            intervalplus,
        } state = init;

        rational lower = 0, upper = 0;

        for(auto x : w){
        switch(state) {
            case init:
                lower = x;
                state = first;
                break;
            case first:
                if(x <= lower) return false;
                upper = x;
                state = interval;
                break;
            case interval:
                if(x <= lower || x >= upper) return false;
                lower = x;
                state = intervalplus;
                break;
            case intervalplus:
                if(x <= lower || x >= upper) return false;
                upper = x;
                state = interval;
                break;
            }
        }
        return w.size() > 0 && w.size() % 2 == 0;
    }
};

/* The language { wa | a = max(w) }, the shortest word accepted is 1 1 (or any
   permutation thereof.
   3 states: 0, 1, 1 */
struct max_lang {
    using alphabet = rational;

    bool is_accepted(std::vector<alphabet> w) const {
        if(w.size() <= 1) return false;
        rational mx = w.front();
        rational last = w.back();
        w.pop_back();

        for(auto x : w) if(x > mx) mx = x;

        return last == mx;
    }
};

/* The language { w | last symbol of w >= any symbol in w }, the empty word
   is not included, because it has no last symbol.
   3 states: 0, 1, 1 */
struct max2_lang {
    using alphabet = rational;

    bool is_accepted(std::vector<alphabet> const & w) const {
        if(w.empty()) return false;
        rational mx = w.front();

        for(auto x : w) if(x > mx) mx = x;

        return w.back() >= mx;
    }
};

/* General wrapper for automata. */
template <typename Q, typename A>
struct from_automaton_t {
    using alphabet = A;
    from_automaton_t(automaton<Q, A> const & aut_) : aut(aut_) {}

    bool is_accepted(std::vector<alphabet> const & w) const {
        return aut.accepts(w);
    }

private:
    automaton<Q, A> aut;
};

template <typename Q, typename A>
from_automaton_t<Q, A> from_automaton(automaton<Q, A> const & aut){
    return from_automaton_t<Q, A>(aut);
}

/* I took the code from the minimisation section, but restricted the alphabet. */
automaton<std::pair<int, abstract>, rational> read_automaton_from_file(std::string const & path){
    using namespace std;
    ifstream in(path);
    string ignore;
    
    automaton<pair<int, abstract>, rational> aut;
    
    int nAlphabet, nStates;
    map<int, orbit<rational>> alphabetMap;
    map<int, orbit<pair<int, abstract>>> stateMap;
    
    // Read header
    in >> ignore >> nAlphabet >> nStates;

    if(nAlphabet != 1)
        throw runtime_error("Alphabet not supported (too few/many orbits)");
    
    // Read alphabet
    in >> ignore;
    for (int i=0; i<nAlphabet; i++) {
        int idx, supportSize;
        in >> idx >> supportSize;
        if(supportSize != 1)
            throw runtime_error("Alphabet not supported (too small/big support)");
        aut.alphabet.insert(rational());
        alphabetMap[idx]; // default constructor
    }
    
    // Read states
    in >> ignore;
    for (int i=0; i<nStates; i++) {
        int idx, supportSize;
        bool isFinal;
        in >> idx >> supportSize >> isFinal;
        orbit<pair<int,abstract>> curOrbit(make_pair(idx, abstract(supportSize)));
        aut.states.orbits.insert(curOrbit);
        stateMap[idx] = curOrbit;
        if (isFinal)
            aut.finalStates.orbits.insert(curOrbit);
        
        // Deal with the fact that the framework needs an initial state.
        if (i == 0) {
            aut.initialState = curOrbit.getElement();
            if(supportSize != 0)
                throw runtime_error("Initial state should have empty support");
        }
    }
    
    // Read delta
    in >> ignore;
    auto deltaDom = nomset_product(aut.states, aut.alphabet);
    int domSize = deltaDom.orbits.size();
    for (int i=0; i<domSize; i++) {
        int state, alph;
        string ProdMap;
        int targ;
        vector<bool> mask;
        in >> state >> alph >> ProdMap >> ignore >> targ;
        mask.resize(ProdMap.size(), false);
        for (size_t i=0; i<ProdMap.size(); i++) {
            char c;
            in >> c;
            if (c == '1')
                mask[i] = true;
        }
        
        pair<pair<int, abstract>, rational> el;
        el.first.first = state;
        for (size_t i=0; i<ProdMap.size(); i++) {
            if (ProdMap[i] != 'B')
                el.first.second.data.insert(rational(i));
            if (ProdMap[i] != 'A')
                el.second = i;
        }
        
        orbit<pair<pair<int,abstract>, rational>> prodOrbit(el);
        aut.delta.mapData[prodOrbit].first = stateMap[targ];
        aut.delta.mapData[prodOrbit].second = mask;
    }

    return aut;
}
