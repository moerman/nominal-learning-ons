#include <fstream>
#include <iomanip>
#include <iostream>
#include <sstream>
#include <string>
#include <vector>

#include <ONS/rational.h>

#include "examples.hpp"
#include "io.hpp"

/* This is a simple program which can answer membership queries automatically.
   The equivalence queries are simply forwarded to stdout, and counter examples
   are then read from stdin.
   The first two command line options are files/sockets to the learning
   algirithm (you can set them to stdin stdout if you wish). Then the last
   parameters describe the automaton you wish to learn.

   Typical automated usage is as (in unix shell):
       mkfifo qs ans
       ./some_learning_algorithm > qs < ans &
       ./external_teacher qs ans Fifo 1 < pre_computed_counter_examples.txt

   For testing this program, I suggest you run:
       ./external_teacher /dev/stdin /dev/stdout Fifo 1
*/

using namespace std;
using namespace ONS;

template <typename AUT>
int run(istream& input, ostream& output, AUT const & automaton_model){
    size_t mq_count = 0, eq_count = 0;
    using alphabet = typename AUT::alphabet;
    using word = vector<alphabet>;
    string q;
    while(input >> q){
        if(q == "ALPHABET") {
            output << alphabet_name<alphabet>::name << endl;
        } else if(q == "MQ") {
            mq_count++;
            string mq;
            input >> quoted(mq);
            const word w = parse_word<alphabet>(mq);
            const bool r = automaton_model.is_accepted(w);
            output << (r ? 'Y' : 'N') << endl;
            // clog << "Answered MQ " << quoted(mq) << " with " << (r ? 'Y' : 'N') << endl;
        } else if (q == "EQ") {
            eq_count++;
            string eq;
            input >> quoted(eq);
            cout << "Can you help me with the EQ:\n" << eq << endl;
            clog << "MQs = " << mq_count << endl;
            clog << "EQs = " << eq_count << endl;
            string user_answer;
            getline(cin, user_answer);
            output << user_answer << endl;
        } else {
            cerr << "Unknown command: " << q << endl;
        }
    }
    clog << "MQs = " << mq_count << endl;
    clog << "EQs = " << eq_count << endl;
}

int main (int argc, char* argv[]) {
    if(argc < 4) return 1;

    /* Rember to flush the outpus (std::endl will do) */
    ifstream input(argv[1]);
    ofstream output(argv[2]);

    string automaton = argv[3];
    if(automaton == "Fifo"){
        if(argc < 5) return 1;
        return run(input, output, fifo(atoi(argv[4])));
    }
    if(automaton == "DoubleWord"){
        if(argc < 5) return 1;
        return run(input, output, double_word(atoi(argv[4]), false));
    }
    if(automaton == "DoubleWordOrd"){
        if(argc < 5) return 1;
        return run(input, output, double_word(atoi(argv[4]), true));
    }
    if(automaton == "NLast"){
        if(argc < 5) return 1;
        return run(input, output, n_last(atoi(argv[4]), false));
    }
    if(automaton == "NLastOrd"){
        if(argc < 5) return 1;
        return run(input, output, n_last(atoi(argv[4]), true));
    }
    if(automaton == "Interval"){
        return run(input, output, interval_lang());
    }
    if(automaton == "Max"){
        return run(input, output, max_lang());
    }
    if(automaton == "Max2"){
        return run(input, output, max2_lang());
    }
    if(automaton == "File"){
        if(argc < 5) return 1;
        return run(input, output, from_automaton(read_automaton_from_file(argv[4])));
    }

    cerr << "Unknown language: " << automaton << endl;
}
