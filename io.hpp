#include <cassert>
#include <iostream>
#include <vector>

#include <ONS/rational.h>

using namespace std;
using namespace ONS;

// assumption: words are presented in a normalised way
bool parse_symbol(istream& in, rational& r){
    int n;
    if(in >> n) {
        r = n;
        return true;
    }
    return false;
}

// special case for Fifo. TODO: make better abstraction
bool parse_symbol(istream& in, pair<bool, rational> & p){
    string str;
    in >> str;
    if(!in) return false;
    if(str != "Put" && str != "Get") return false;
    rational r;
    if(!parse_symbol(in, r)) return false;
    p = make_pair(str == "Get", r);
    return true;
}

template <typename A>
vector<A> parse_word(string mq){
    stringstream ss(mq);
    vector<A> w;
    A n;
    while(parse_symbol(ss, n)) w.push_back(n);
    return w;
}

void write_symbol(ostream& out, rational const & r){
    /* Currently, all words generated are integral. Since a word is
       finite, this can always be ensured. Here we assume it wlog.
       This makes it a bit easier to parse for other tools. */
    assert(r.getDenominator() == 1);
    out << r.getNumerator();
}

// special case for Fifo. TODO: make better abstraction
void write_symbol(ostream& out, pair<bool, rational> const & p){
    if(!p.first) {
        out << "Put ";
    } else {
        out << "Get ";
    }
    write_symbol(out, p.second);
}

template <typename A>
void write_word(ostream& out, vector<A> const & w){
    for (int i = 0; i < w.size(); ++i) {
        if (i > 0) out << ' ';
        write_symbol(out, w[i]);
    }
}
