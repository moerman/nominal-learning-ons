Additional files for L-star in NLambda
======================================

In order to run the NLambda implementation with the external teacher in this
repository, we supply some additional files.
First, there is the `stack.yaml` file, which contains some information for
building the tool. You should set the directories to where you cloned NLambda
and its L-star implementation. And you should choose which symmetry to use,
this is done by setting the flag to true (total order) or false (equality).
If you setup the dirs and flags, run `stack build`.
Second, there is `run.sh` which passes the right arguments to the learner and
sets up the communication channels, it can be run from here.

You will need [NLambda](https://github.com/szynwelski/nlambda) and the
[L-star implementation](https://github.com/Jaxan/nominal-lstar).
