#include <iostream>
#include <stdexcept>
#include <sstream>
#include <string>
#include <type_traits>
#include <vector>

#include <ONS/eqimap.h>
#include <ONS/nomset.h>
#include <ONS/orbit.h>

#include "automata.hpp"
#include "io.hpp"
#include "ONS_io.hpp"
#include "ONS_nomset_extra.hpp"
#include "ONS_quotients.hpp"

using namespace std;
using namespace ONS;

const bool add_suffixes_of_counter_example = true;

/* Currently, only supports an I/O teacher */
template <typename A>
bool is_member_io(std::vector<A> const & word) {
    std::cout << "MQ \"";
    write_word(std::cout, word);
    std::cout << "\"" << endl;

    std::string answer;
    cin >> answer;
    if (answer == "Y") return true;
    else if (answer == "N") return false;
    else throw runtime_error("in is_member_io: couldn't interpret: " + answer);
}

// emulating an optional type here
template <typename Q, typename A>
std::pair<bool, std::vector<A>> is_equivalent_io(automaton<Q, A> const & aut) {
    std::cout << "EQ \"";
    std::cout << "Is the following automaton correct? [Y/counter-example]" << std::endl;
    std::cout << "States =\n\t" << aut.states << std::endl;
    std::cout << "Transition =\n\t" << aut.delta << std::endl;
    std::cout << "Final states =\n\t" << aut.finalStates << std::endl;
    std::cout << "Initial state =\n\t" << aut.initialState << std::endl;
    std::cout << "\"" << endl;

    std::string answer;
    std::cin >> answer;
    if(answer == "Y") {
        return {true, {}};
    } else {
        assert(answer == "N");
        std::string line;
        std::getline(cin, line);
        std::vector<A> w = parse_word<A>(line);
        return {false, w};
    }
}

template <typename A>
struct alphabet_sets {
    static const nomset<A> set;
};

template <>
const nomset<rational> alphabet_sets<rational>::set
    = make_set(rational());

template <>
const nomset<pair<bool, rational>> alphabet_sets<pair<bool, rational>>::set
    = nomset_union(make_set(make_pair(false, rational())), make_set(make_pair(true, rational())));


/* I removed the optimisation for the equality symmetry. (It's only an
   optimisation in terms of membership queries, not runtime. */
template <typename A>
struct ExternalTeacher {
    ExternalTeacher() = default;

    nomset<A> alphabet() {
        return alphabet_sets<A>::set;
    }

    bool is_member(std::vector<A> const & w){
        if (cache.inDomain(w)) {
            return cache(w);
        }

        const auto b = is_member_io(w);
        cache.add(w, b);
        return b;
    }

    template <typename Q>
    std::pair<bool, std::vector<A>> is_equivalent(automaton<Q, A> const & aut) {
        return is_equivalent_io(aut);
    }

private:
    eqimap<std::vector<A>, bool> cache;
};

template <typename A>
void learn () {
    using word = std::vector<A>;
    const word empty_word = {};

    ExternalTeacher<A> teacher;
    nomset<A> alphabet_set = teacher.alphabet();

    /* This is all the state we update during the L* algorithm. */
    nomset<word> prefs = make_set(empty_word);
    nomset<word> prefs_ext;
    nomset<word> suffs = make_set(empty_word);
    eqimap<std::pair<word, word>, bool> table;

    /* We initialise the data structures. */
    for (const auto ao : alphabet_set) {
        prefs_ext.insert(word(1, ao.getElement()));
    }

    for (const auto & p_orbit : nomset_product(nomset_union(prefs, prefs_ext), suffs)) {
        const auto & p = p_orbit.getElement();
        const word w = concat(p.first, p.second);
        const auto b = teacher.is_member(w);
        table.add(p, b);
    }

    /* Function to add rows. Precondition: new_prefixes plus prefs is prefix-closed. */
    const auto add_rows = [&](nomset<word> const & new_prefixes) {
        clog << "Adding rows: " << new_prefixes << endl;
        /* Computing the one-letter-extensions. */
        nomset<word> new_words;
        for (const auto & p_orbit : nomset_product(new_prefixes, alphabet_set)) {
            const auto & p = p_orbit.getElement();
            const word w = concat(p.first, p.second);
            new_words.insert(w);
        }

        /* Concatenating them with the columns to pose MQs. */
        for (const auto & p_orbit : nomset_product(new_words, suffs)) {
            const auto & p = p_orbit.getElement();
            const word w = concat(p.first, p.second);
            const auto b = teacher.is_member(w);
            table.add(p, b);
        }

        /* Update our sets of words accordingly. We keep prefs_ext small. */
        prefs = nomset_union(prefs, new_prefixes);
        prefs_ext = nomset_union(prefs_ext, new_words);
        prefs_ext = nomset_minus(prefs_ext, prefs);
    };

    /* Function to add columns. */
    const auto add_cols = [&](nomset<word> const & new_suffices) {
        clog << "Adding columns: " << new_suffices << endl;
        /* We combine all prefixes with the new suffix. And fill
           the table with MQs. */
        for (const auto & p_orbit : nomset_product(nomset_union(prefs, prefs_ext), new_suffices)) {
            const auto & p = p_orbit.getElement();
            const word w = concat(p.first, p.second);

            // ask membership query
            const auto b = teacher.is_member(w);
            // add to table
            table.add(p, b);
        }

        suffs = nomset_union(suffs, new_suffices);
    };

    /* This is the main learning loop. */
    while(true) {
        /* First we will check for closedness of the table. That means that
           every row in `prefs_ext` should be represented by a row in `prefs`.
           If some are not represented, we extend the table with those, and
           repeat the procedure. The set `closedness` will contain the rows
           which are represented. */
        nomset<word> closedness;
        eqimap<word, word> transition;
        bool closed = true;
        for (const auto & sas : nomset_product(prefs_ext, prefs)) {
            /* We can skip the row if we already know it is represented */
            if (closedness.contains(sas.getElement().first)) {
                continue;
            }

            /* If the table is complete, we will find representatives with
               a support smaller or equal to that of the one-letter-extension.
               So if we have elements outside the support of the word we are
               considering, we can skip it. */
            const auto sas_el = sas.getElement();
            if (orbit<word>(sas_el.first).supportSize() < sas.supportSize()) {
                continue;
            }

            /* Then, we will check all columns, if there is no difference
               the current row in `prefs_ext` is represented by a row in
               `prefs`. And then we add it to `closedness`. */
            bool equal = true;
            for (const auto & sase_orbit : nomset_product(make_set(sas), suffs)) {
                const auto & sase = sase_orbit.getElement();
                if (table(make_pair(sase.first.first, sase.second)) !=
                    table(make_pair(sase.first.second, sase.second))) {
                    equal = false;
                }
            }

            if (equal) {
                transition.add(sas_el.first, sas_el.second);
                closedness.insert(sas.getElement().first);
            }
        }

        /* The table is closed if all rows in `prefs_ext` are represented/ */
        if (closedness.contains(prefs_ext)) {
            clog << "The table is closed" << endl;
        } else {
            /* Otherwise, we can add the rows which are not represented. For
               the new one-letter-extensions of these rows, we pose MQs. */
            closed = false;
            nomset<word> incompleteness = nomset_minus(prefs_ext, closedness);

            auto inc2 = make_set(*incompleteness.begin());
            add_rows(inc2);
        }

        /* Next we will check for consistency. We iterate over all pairs of
           rows in `prefs`. Then such a pair is a candidate if they are
           equivalent. For the equivalent ones, we check their one-letter-
           extensions. */
        bool consistent = true;
        nomset<pair<word, word>> equivalence;
        for (const auto & ss_orbit : nomset_product(prefs, prefs)) {
            const auto & ss = ss_orbit.getElement();
            /* We can ignore a pair (x, x), because it is always consitent. */
            if (ss.first == ss.second) {
                equivalence.insert(ss);
                continue;
            }

            /* Then we will determine whether the two rows are equivalent. */
            bool equal = true;
            for (const auto & sse_orbit : nomset_product(make_set(ss_orbit), suffs)) {
                const auto & sse = sse_orbit.getElement();
                if (table(make_pair(sse.first.first, sse.second)) !=
                    table(make_pair(sse.first.second, sse.second))) {
                    equal = false;
                    break;
                }
            }

            /* If the two rows are inequivalent, they are not an inconsistency
               candidate. So we continue the search. */
            if (!equal) {
                continue;
            }

            equivalence.insert(ss);

            /* Now that we have a candidate, we check the one-letter extensions.
               If these are inequivelent, we have an inconsistency. We will
               immediately fix the inconsistency. */
            for (const auto & ssae_orbit : nomset_product(make_set(ss_orbit), nomset_product(alphabet_set, suffs))) {
                const auto & ssae = ssae_orbit.getElement();
                const auto & a = ssae.second.first;     // one-letter-extension
                const auto & e = ssae.second.second;    // column
                word w1 = concat(ssae.first.first, a);  // first row, extended
                word w2 = concat(ssae.first.second, a); // second row, extended

                /* If equivalent, continue searching. */
                if (table(make_pair(w1, e)) ==
                    table(make_pair(w2, e))) {
                    continue;
                }

                /* Else: we found an inconsistency! */
                consistent = false;

                /* Create new column. */
                const word extension = concat(a, e);
                add_cols(make_set(extension));

                /* We can stop. */
                break;
            }
            
            /* We can stop as soon as we find an inconsistency. */
            if (!consistent) break;
        }

        /* Jump back to main loop. */
        if (!consistent || !closed) continue;

        clog << "The table is consistent" << endl;

        /* Ok now we are ready to build the hypothesis. In the previous checks
           we built up some information which we can now use. For example, the
           equivalence of states has already been computed for consistency. So
           we can simply build the quotient automaton, right away! */
        const auto q = partition_from_eqrel(equivalence, prefs);
        const auto states = q.first;
        const auto partition_map = q.second;

        /* Initial state is given by the empty word. */
        const auto initial_state = partition_map(empty_word);
        using state_type = typename std::remove_const<decltype(initial_state)>::type;

        /* Final states are determined with the empty-word-column. */
        nomset<state_type> final_states;
        for (const auto & p_orbit : prefs) {
            const auto & p = p_orbit.getElement();
            if (table(make_pair(p, empty_word))) {
                final_states.insert(partition_map(p));
            }
        }

        /* Most interstingly: the transitions. This is as simple as
           delta([s], a) = [sa]. We iterate over the prefixes so that we have
           representatives for all states. */
        eqimap<pair<state_type, A>, state_type> delta;
        for (const auto p_orbit : nomset_product(prefs, alphabet_set)) {
            const auto p = p_orbit.getElement();

            const auto state = partition_map(p.first);
            const auto a = p.second;

            const word w = concat(p.first, a);

            if (prefs_ext.contains(w)) {
                delta.add(make_pair(state, a), partition_map(transition(w)));
            } else {
                delta.add(make_pair(state, a), partition_map(w));
            }
        }

	const auto aut = automaton<state_type, A>{alphabet_set, states, delta, final_states, initial_state};

        askAgain:
        /* Then we pass the hypothesis on to the teacher. */
        const auto res = teacher.is_equivalent(aut);
        if (res.first) {
            clog << "Finished learning!" << endl;
            break;
        }

        /* Else we get a counter example to analyse. We will add all prefixes
           to the table and reiterate the main loop. */
        word counterexample = res.second;

        bool ans1 = teacher.is_member(counterexample);
        bool ans2 = aut.accepts(counterexample);
        if(ans1 == ans2) goto askAgain;

        if(add_suffixes_of_counter_example) {
            word suffix = empty_word;
            nomset<word> new_suffs;
            for(auto it = counterexample.rbegin(); it != counterexample.rend(); ++it) {
                suffix.insert(suffix.begin(), *it); // TODO: replace by deque
                if(suffs.contains(suffix)) continue;
                new_suffs.insert(suffix);
            }
            
            add_cols(new_suffs);
        } else {
            word prefix = empty_word;
            nomset<word> new_prefs;
            for(auto a : counterexample) {
                prefix.push_back(a);
                if(prefs.contains(prefix)) continue;
                new_prefs.insert(prefix);
            }
            
            add_rows(new_prefs);
        }

        continue;
    }
}

int main() {
    cout << "ALPHABET" << endl;
    string alph;
    cin >> alph;

    if(alph == "ATOMS") {
        learn<rational>();
    } else if (alph == "FIFO") {
        learn<pair<bool, rational>>();
    } else {
        cerr << "Unknown alphabet " << alph << endl;
        return 1;
    }
}

