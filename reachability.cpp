#include <iostream>
#include <algorithm>
#include <vector>
#include <queue>

#include <ONS/orbit.h>
#include <ONS/eqimap.h>
#include <ONS/nomset.h>
#include <ONS/rational.h>
#include "ONS_nomset_extra.hpp"

#include "automata.hpp"
#include "examples.hpp"

/* This program computes paths leading to each state of the automaton. If
   provided multiple input automata, it will take the union of all thoses sets.
   This can be used to generate (weak) counterexamples for the learning
   algorithm. It could provide a basis on which someon can implement a W-method
   for nominal automata :-).
*/

using namespace std;
using namespace ONS;

void print(vector<rational> const & v){
	cout << 'N';
	for(auto x : v) cout << ' ' << x.getNumerator();
	cout << endl;
}

int main(int argc, char **argv) {
    using state = pair<int, abstract>;
    using alphabet = rational;
    nomset<rational> alphabet_set = make_set(rational());

    nomset<vector<alphabet>> all_words;

    for (int i=1; i<argc; i++) {
        automaton<state, rational> aut = read_automaton_from_file(argv[i]);

        nomset<state> seen;
        queue<pair<state, vector<alphabet>>> work;

        work.push(make_pair(aut.initialState, vector<alphabet>{}));

        while(!work.empty()) {
            auto sw = work.front();
            work.pop();

            if(seen.contains(sw.first)) continue;
            seen.insert(sw.first);
            all_words.insert(sw.second);

            for(auto adj_o : nomset_product(make_set(sw), alphabet_set)) {
                auto adj = adj_o.getElement();
                auto t = aut.delta({adj.first.first, adj.second});
                if(seen.contains(t)) continue;

                auto w2 = concat(adj.first.second, adj.second);
                all_words.insert(w2);
                work.push({t, w2});
            }
        }
    }

    vector<vector<alphabet>> all_words_v;
    for(auto w_o : all_words) {
        auto w = w_o.getElement();
        all_words_v.push_back(w);
    }

    sort(all_words_v.begin(), all_words_v.end(), [](auto l, auto r) { return l.size() < r.size(); });

    for (auto w : all_words_v){
        if(w.empty()) continue;
        print(w);
    }
    cout << 'Y' << endl;
}
