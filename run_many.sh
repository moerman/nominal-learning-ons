#!/bin/bash

timeout 60m ./run.sh Fifo 1 < counterexamples/cex_fifo_1.txt > ../log/ons_fifo_1.txt 2>&1
timeout 60m ./run.sh Fifo 2 < counterexamples/cex_fifo_2.txt > ../log/ons_fifo_2.txt 2>&1
timeout 60m ./run.sh Fifo 3 < counterexamples/cex_fifo_3.txt > ../log/ons_fifo_3.txt 2>&1
timeout 60m ./run.sh DoubleWord 1 < counterexamples/cex_doubleword_1.txt > ../log/ons_doubleword_1.txt 2>&1
timeout 60m ./run.sh DoubleWord 2 < counterexamples/cex_doubleword_2.txt > ../log/ons_doubleword_2.txt 2>&1
timeout 60m ./run.sh DoubleWord 3 < counterexamples/cex_doubleword_3.txt > ../log/ons_doubleword_3.txt 2>&1
timeout 60m ./run.sh File random-automata/random_5_1_1.txt < counterexamples/cex_random_automata.txt > ../log/ons_random_5_1_1.txt 2>&1
timeout 60m ./run.sh File random-automata/random_5_1_2.txt < counterexamples/cex_random_automata.txt > ../log/ons_random_5_1_2.txt 2>&1
timeout 60m ./run.sh File random-automata/random_5_1_3.txt < counterexamples/cex_random_automata.txt > ../log/ons_random_5_1_3.txt 2>&1
timeout 60m ./run.sh File random-automata/random_5_1_4.txt < counterexamples/cex_random_automata.txt > ../log/ons_random_5_1_4.txt 2>&1
timeout 60m ./run.sh File random-automata/random_5_1_5.txt < counterexamples/cex_random_automata.txt > ../log/ons_random_5_1_5.txt 2>&1
timeout 60m ./run.sh Max < counterexamples/cex_max.txt > ../log/ons_max.txt 2>&1
timeout 60m ./run.sh Interval < counterexamples/cex_interval.txt > ../log/ons_interval.txt 2>&1
